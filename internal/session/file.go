package session

import (
	"encoding/json"
	"errors"
	"os"
	"sync"
)

type FileSession struct {
	filePath    string
	sessionData map[string]string
	mu          sync.Mutex
}

func NewFileSession(filePath string) ISession {
	return &FileSession{
		filePath:    filePath,
		sessionData: make(map[string]string),
	}
}

func (s *FileSession) Load() error {
	s.mu.Lock()
	defer s.mu.Unlock()

	file, err := os.Open(s.filePath)
	if err != nil {
		if os.IsNotExist(err) {
			return nil // No session file, no problem.
		}
		return err
	}
	defer file.Close()

	decoder := json.NewDecoder(file)
	return decoder.Decode(&s.sessionData)
}

func (s *FileSession) Save() error {
	s.mu.Lock()
	defer s.mu.Unlock()

	file, err := os.Create(s.filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	encoder := json.NewEncoder(file)
	return encoder.Encode(&s.sessionData)
}

func (s *FileSession) Set(key string, value string) {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.sessionData[key] = value
}

func (s *FileSession) Get(key string) (string, error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	value, exists := s.sessionData[key]
	if !exists {
		return "", errors.New("key does not exist in session")
	}
	return value, nil
}

func (s *FileSession) Delete(key string) {
	s.mu.Lock()
	defer s.mu.Unlock()
	delete(s.sessionData, key)
}

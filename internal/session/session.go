package session

type ISession interface {
	Load() error
	Save() error
	Set(key string, value string)
	Get(key string) (string, error)
	Delete(key string)
}

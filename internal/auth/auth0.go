package auth

import (
	"context"
	"fmt"
	"net/http"
	"sync"

	"github.com/coreos/go-oidc/v3/oidc"
	"golang.org/x/oauth2"

	"cobra-test/internal/session"
)

type tokenClaims struct {
	Name     string `json:"Name"`
	Nickname string `json:"nickname"`
}

type Authenticator struct {
	domain string
	*oidc.Provider
	oauth2.Config
}

func (a Authenticator) GetLoginUrl(state string) string {
	return a.AuthCodeURL(state)
}

func (a Authenticator) HandleCallback(
	ctx context.Context,
	w http.ResponseWriter,
	r *http.Request,
	state string,
	wg *sync.WaitGroup,
	session session.ISession,
) {
	defer wg.Done()

	if r.URL.Query().Get("state") != state {
		http.Error(w, "State does not match", http.StatusBadRequest)
		return
	}

	oauth2Token, err := a.Exchange(ctx, r.URL.Query().Get("code"))
	if err != nil {
		http.Error(w, "Failed to exchange token: "+err.Error(), http.StatusInternalServerError)
		return
	}

	rawIDToken, ok := oauth2Token.Extra("id_token").(string)
	if !ok {
		http.Error(w, "No id_token field in oauth2 token", http.StatusInternalServerError)
		return
	}

	idToken, err := a.Verifier(&oidc.Config{ClientID: a.ClientID}).Verify(ctx, rawIDToken)
	if err != nil {
		http.Error(w, "Failed to verify ID token: "+err.Error(), http.StatusInternalServerError)
		return
	}

	var claims tokenClaims

	if err = idToken.Claims(&claims); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	session.Set("id_token", rawIDToken)
	if err = session.Save(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	if _, err = fmt.Fprintf(w, "Login success. You can close this tab and continue"); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (a Authenticator) IsAuthenticated(ctx context.Context, rawIDToken string) bool {
	if rawIDToken == "" {
		return false
	}

	idToken, err := a.Verifier(&oidc.Config{ClientID: a.ClientID}).Verify(ctx, rawIDToken)
	if err != nil {
		return false
	}

	var claims tokenClaims

	if err = idToken.Claims(&claims); err != nil {
		return false
	}

	return true
}

func (a Authenticator) LogoutUrl(rawIDToken string) string {
	return fmt.Sprintf("https://%s/oidc/logout?client_id=%s&id_token_hint=%s", a.domain, a.ClientID, rawIDToken)
}

func NewAuth0Provider(domain, clientId, clientSecret, callbackUrl string) (IProvider, error) {
	provider, err := oidc.NewProvider(
		context.Background(),
		"https://"+domain+"/",
	)
	if err != nil {
		return nil, err
	}

	conf := oauth2.Config{
		ClientID:     clientId,
		ClientSecret: clientSecret,
		RedirectURL:  callbackUrl,
		Endpoint:     provider.Endpoint(),
		Scopes:       []string{oidc.ScopeOpenID, "profile"},
	}

	return &Authenticator{
		Provider: provider,
		Config:   conf,
		domain:   domain,
	}, nil
}

package auth

import (
	"context"
	"net/http"
	"sync"

	"cobra-test/internal/session"
)

type IProvider interface {
	GetLoginUrl(state string) string
	HandleCallback(
		ctx context.Context,
		w http.ResponseWriter,
		r *http.Request,
		state string,
		wg *sync.WaitGroup,
		session session.ISession,
	)
	IsAuthenticated(ctx context.Context, rawIDToken string) bool
	LogoutUrl(rawIDToken string) string
}

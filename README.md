# BB home assignment

### Please find executable in `dist` folder

Possible issues on MacOS and how to solve them:
- If you have `operation not permitted` error - run this commands:
```
chmod +x ./bb-test
xattr -d com.apple.quarantine ./bb-test
```

### Credentials to use on login:
Username: `serhii.butenko@rolique.io`

Password: `Qwerty1234!`

### Available commands:

Login to application:

```
./bb-test login
```

Check authentication status:
```
./bb-test status
```

Logout:
```
./bb-test logout
```

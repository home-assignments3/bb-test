package cmd

import (
	"github.com/fatih/color"
	"github.com/spf13/cobra"
)

var (
	logoutCmd = &cobra.Command{
		Use:   "logout",
		Short: "Logout user",
		RunE: func(cmd *cobra.Command, args []string) error {
			rawIDToken, err := appSession.Get("id_token")
			if err != nil {
				return nil
			}
			if rawIDToken == "" {
				return nil
			}

			appSession.Delete("id_token")
			if err = appSession.Save(); err != nil {
				return errorf("could not save session: %v", err)
			}

			logoutUrl := authProvider.LogoutUrl(rawIDToken)

			if err := openBrowser(logoutUrl); err != nil {
				return errorf("failed to open browser: %s", err)
			}

			color.Green("Success logout")

			return nil
		},
	}
)

package cmd

import (
	"crypto/rand"
	"encoding/base64"
	"log"
	"net/http"
	"os/exec"
	"runtime"
	"sync"

	"github.com/fatih/color"
	"github.com/spf13/cobra"
)

const (
	OperatingSystemLinux   string = "linux"
	OperatingSystemWindows string = "windows"
	OperatingSystemMacOS   string = "darwin"
)

var (
	loginCmd = &cobra.Command{
		Use:   "login",
		Short: "Authenticate user",
		RunE: func(cmd *cobra.Command, args []string) error {
			if isAuthenticated(cmd.Context()) {
				color.Green("Already authenticated")

				return nil
			}

			state, err := generateRandomState()
			if err != nil {
				return errorf("failed to generate random state: %s", err)
			}

			var wg sync.WaitGroup

			wg.Add(1)

			http.HandleFunc("/callback", func(w http.ResponseWriter, r *http.Request) {
				authProvider.HandleCallback(cmd.Context(), w, r, state, &wg, appSession)
			})

			// Start the web server to handle the callback
			go func() {
				if err = http.ListenAndServe(":3000", nil); err != nil {
					log.Fatalf("failed to start server: %v", err)
				}
			}()

			color.Green("Opening browser for authentication...")
			if err := openBrowser(authProvider.GetLoginUrl(state)); err != nil {
				return errorf("failed to open browser: %s", err)
			}

			wg.Wait()

			return nil
		},
	}
)

func generateRandomState() (string, error) {
	b := make([]byte, 32)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}

	state := base64.StdEncoding.EncodeToString(b)

	return state, nil
}

func openBrowser(url string) error {
	switch runtime.GOOS {
	case OperatingSystemLinux:
		return exec.Command("xdg-open", url).Start()
	case OperatingSystemWindows:
		return exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case OperatingSystemMacOS:
		return exec.Command("open", url).Start()
	default:
		return errorf("unsupported platform")
	}
}

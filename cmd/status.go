package cmd

import (
	"context"

	"github.com/fatih/color"
	"github.com/spf13/cobra"
)

var (
	statusCmd = &cobra.Command{
		Use:   "status",
		Short: "Checks if user authenticated",
		Run: func(cmd *cobra.Command, args []string) {
			if isAuthenticated(cmd.Context()) {
				color.Green("User is authenticated")
				return
			}

			color.Red("User is NOT authenticated")
		},
	}
)

func isAuthenticated(ctx context.Context) bool {
	rawIDToken, err := appSession.Get("id_token")
	if err != nil {
		return false
	}

	if !authProvider.IsAuthenticated(ctx, rawIDToken) {
		return false
	}

	return true
}

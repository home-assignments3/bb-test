package cmd

import (
	"log"
	"os"
	"path/filepath"

	"github.com/fatih/color"
	"github.com/spf13/cobra"

	"cobra-test/internal/auth"
	"cobra-test/internal/session"
)

var (
	rootCmd = &cobra.Command{
		Use:   "bb",
		Short: "BB home assignment",
		Run:   func(cmd *cobra.Command, args []string) {},
	}

	appSession   session.ISession
	authProvider auth.IProvider

	auth0Domain       string
	auth0ClientId     string
	auth0ClientSecret string
	auth0CallbackUrl  string
)

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func errorf(format string, a ...interface{}) error {
	return errorf(color.RedString(format), a...)
}

func init() {
	// Initialize session
	sessionFile := filepath.Join("/tmp", "myapp_session.json")
	appSession = session.NewFileSession(sessionFile)

	// Load session
	if err := appSession.Load(); err != nil {
		log.Fatalf("Failed to load session: %v", err)
	}

	// Ensure session is saved on exit
	defer func() {
		if err := appSession.Save(); err != nil {
			log.Fatalf("Failed to save session: %v", err)
		}
	}()

	rootCmd.AddCommand(loginCmd)
	rootCmd.AddCommand(logoutCmd)
	rootCmd.AddCommand(statusCmd)

	rootCmd.PersistentFlags().StringVar(&auth0Domain, "auth0_domain", "dev-jtktedh8qdm0s17r.us.auth0.com", "AUTH0 Domain")
	rootCmd.PersistentFlags().StringVar(&auth0ClientId, "auth0_client_id", "NZLXEWIjZjnYWZoF78uF5qbrb2FP9Dqo", "AUTH0 client id")
	rootCmd.PersistentFlags().StringVar(&auth0ClientSecret, "auth0_client_secret", "Cw1kjTQjnqZD2j_wYpB0lQV5AHLYuP0yCAqMnv0YUMi3dpfs4H-ClvR4mYGt-8-s", "AUTH0 client secret")
	rootCmd.PersistentFlags().StringVar(&auth0CallbackUrl, "auth0_callback_url", "http://localhost:3000/callback", "AUTH0 callback url")

	auth0Provider, err := auth.NewAuth0Provider(auth0Domain, auth0ClientId, auth0ClientSecret, auth0CallbackUrl)
	if err != nil {
		log.Fatalf("Failed to initialize Auth0 provider: %v", err)
	}
	authProvider = auth0Provider
}
